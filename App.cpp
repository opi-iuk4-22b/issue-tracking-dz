#include "App.h"
#include "InterfaceMenu.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <ctime>
using namespace std;

void addClient() {
	setlocale(LC_ALL, "");
	cout << "Введите имя клиента: ";
	string login;
	cin >> login;
	cout << "Введите пароль клиента: ";
	string password;
	cin >> password;
	string nameFile = "ClientList.bin";
	ofstream clientAddFile(nameFile, ios::binary | ios::app);
	Client client{ login, password };
	clientAddFile.write((char*)&client, sizeof(Client));
	clientAddFile.close();
	ViewClientMenu();
}

void addWorker() {
	setlocale(LC_ALL, "");
	cout << "Введите имя сотрудника: ";
	string login;
	cin >> login;
	cout << "Введите пароль сотрудника: ";
	string password;
	cin >> password;
	cout << "Введите должность сотрудника: ";
	string position;
	cin >> position;
	cout << "Введите сумму зарплаты: ";
	double salary;
	cin >> salary;
	string nameFile = "WorkerList.bin";
	ofstream workerAddFile(nameFile, ios::binary | ios::app);
	Worker worker{ login, password, position, salary };
	workerAddFile.write((char*)&worker, sizeof(Worker));
	workerAddFile.close();
	ViewWorkerMenu();
}

void addCarpet() {
	setlocale(LC_ALL, "");
	cout << "Введите название товара: ";
	string name;
	cin >> name;
	cout << "Введите описание товара: ";
	string describe;
	cin >> describe;
	cout << "Введите цену товара: ";
	double price;
	cin >> price;
	cout << "Введите количество товара: ";
	int number;
	cin >> number;
	bool isExist = false;
	if (number > 0) { isExist = true; }
	string nameFile = "CarpetList.bin";
	ofstream carpetAddFile(nameFile, ios::binary | ios::app);
	Carpet carpet{ name, describe, price, rand() % 9000, number, isExist };
	carpetAddFile.write((char*)&carpet, sizeof(Carpet));
	carpetAddFile.close();
	ViewCarpetMenu();
}

void addOrder() {
	setlocale(LC_ALL, "");
	cout << "Введите id пользователя: ";
	int idUser;
	cin >> idUser;
	cout << "Введите id товара: ";
	int idCarpet;
	cin >> idCarpet;
	cout << "Введите количество товара: ";
	int kol;
	cin >> kol;
	string nameFile = "OrderList.bin";
	ofstream orderAddFile(nameFile, ios::binary | ios::app);
	Order order{ rand() % 9000, idUser, idCarpet, time(0), kol };
	orderAddFile.write((char*)&order, sizeof(Order));
	orderAddFile.close();
	ViewOrderMenu();
}

void removeClient() {
	setlocale(LC_ALL, "");
	cout << "Введите id удаляемого клиента: ";
	int number;
	cin >> number;
	Client deleteClient{ (string)"", (string)"" };
	string nameFile = "ClientList.bin";
	string nameAddFile = "ClientListAdd.bin";
	ofstream clientDelFileAdd(nameAddFile, ios::binary | ios::out);
	fstream clientDelFile(nameFile, ios::binary | ios::in);
	while (clientDelFile.read((char*)&deleteClient, sizeof(Client))) {
		if (deleteClient.getID() != number) {
			clientDelFileAdd.write((char*)&deleteClient, sizeof(Client));
		}
	}
	clientDelFile.close();
	clientDelFileAdd.close();

	ofstream outclientDelFile(nameFile, ios::binary | ios::out);
	fstream inclientDelFileAdd(nameAddFile, ios::binary | ios::in);
	while (inclientDelFileAdd.read((char*)&deleteClient, sizeof(Client))) {
		outclientDelFile.write((char*)&deleteClient, sizeof(Client));
	}
	outclientDelFile.close();
	inclientDelFileAdd.close();
	ViewClientMenu();
}

void removeWorker() {
	setlocale(LC_ALL, "");
	cout << "Введите id удаляемого сотрудника: ";
	int number;
	cin >> number;
	Worker deleteWorker{ (string)"", (string)"", (string)"", 0.0 };
	string nameFile = "WorkerList.bin";
	string nameAddFile = "WorkerListAdd.bin";
	ofstream workerDelFileAdd(nameAddFile, ios::binary | ios::out);
	fstream workerDelFile(nameFile, ios::binary | ios::in);
	while (workerDelFile.read((char*)&deleteWorker, sizeof(Worker))) {
		if (deleteWorker.getID() != number) {
			workerDelFileAdd.write((char*)&deleteWorker, sizeof(Worker));
		}
	}
	workerDelFile.close();
	workerDelFileAdd.close();

	ofstream outworkerDelFile(nameFile, ios::binary | ios::out);
	fstream inworkerDelFileAdd(nameAddFile, ios::binary | ios::in);
	while (inworkerDelFileAdd.read((char*)&deleteWorker, sizeof(Worker))) {
		outworkerDelFile.write((char*)&deleteWorker, sizeof(Worker));
	}
	outworkerDelFile.close();
	inworkerDelFileAdd.close();
	ViewWorkerMenu();
}

void removeCarpet() {
	setlocale(LC_ALL, "");
	cout << "Введите id удаляемого товара: ";
	int number;
	cin >> number;
	Carpet deleteCarpet{ (string)"", (string)"", 0.0, rand() % 9000, 0, false };
	string nameFile = "CarpetList.bin";
	string nameAddFile = "CarpetList.bin";
	ofstream carpetDelFileAdd(nameAddFile, ios::binary | ios::out);
	fstream carpetDelFile(nameFile, ios::binary | ios::in);
	while (carpetDelFile.read((char*)&deleteCarpet, sizeof(Carpet))) {
		if (deleteCarpet.m_id != number) {
			carpetDelFileAdd.write((char*)&deleteCarpet, sizeof(Carpet));
		}
	}
	carpetDelFile.close();
	carpetDelFileAdd.close();

	ofstream outcarpetDelFile(nameFile, ios::binary | ios::out);
	fstream incarpetDelFileAdd(nameAddFile, ios::binary | ios::in);
	while (incarpetDelFileAdd.read((char*)&deleteCarpet, sizeof(Carpet))) {
		outcarpetDelFile.write((char*)&deleteCarpet, sizeof(Carpet));
	}
	outcarpetDelFile.close();
	incarpetDelFileAdd.close();
	ViewCarpetMenu();
}

void removeOrder() {
	setlocale(LC_ALL, "");
	cout << "Введите id удаляемого товара: ";
	int number;
	cin >> number;
	Order deleteOrder{ 0,0,0,time(0),0 };
	string nameFile = "CarpetList.bin";
	string nameAddFile = "CarpetList.bin";
	ofstream orderDelFileAdd(nameAddFile, ios::binary | ios::out);
	fstream orderDelFile(nameFile, ios::binary | ios::in);
	while (orderDelFile.read((char*)&deleteOrder, sizeof(Order))) {
		if (deleteOrder.m_id != number) {
			orderDelFileAdd.write((char*)&deleteOrder, sizeof(Order));
		}
	}
	orderDelFile.close();
	orderDelFileAdd.close();

	ofstream outorderDelFile(nameFile, ios::binary | ios::out);
	fstream inorderDelFileAdd(nameAddFile, ios::binary | ios::in);
	while (inorderDelFileAdd.read((char*)&deleteOrder, sizeof(Order))) {
		outorderDelFile.write((char*)&deleteOrder, sizeof(Order));
	}
	outorderDelFile.close();
	inorderDelFileAdd.close();
	ViewOrderMenu();
}

void sortClient() {
	int size = 0;
	string nameFile = "ClientList.bin";
	Client addClient{ (string)"", (string)"" };
	fstream readIn(nameFile, ios::binary | ios::in);
	while (readIn.read((char*)&addClient, sizeof(Client))) {
		size++;
	}
	readIn.close();

	Client* temp = new Client[size];
	int i = 0;
	fstream readList(nameFile, ios::binary | ios::in);
	while (readList.read((char*)&addClient, sizeof(Client))) {
		temp[i] = addClient;
		i++;
	}
	readList.close();

	for (int i = 0; i < size; i++) {
		bool flag = true;
		for (int j = 0; j < size - (i + 1); j++) {
			if (temp[j].getLogin() > temp[j + 1].getLogin()) {
				flag = false;
				swap(temp[j], temp[j + 1]);
			}
		}
		if (flag) { break; }
	}
	for (int j = 0; j < size; j++) {
		cout << "\nID - " << temp[j].getID() << "\tИмя - " << temp[j].getLogin() << endl;
	}
	int request;
	cout << "Чтобы вернуться назад введите любую цифру...";
	cin >> request;
	ViewClientMenu();
}

void sortWorker() {
	int size = 0;
	string nameFile = "WorkerList.bin";
	Worker addWorker{ (string)"", (string)"", (string)"", 0.0 };
	fstream readIn(nameFile, ios::binary | ios::in);
	while (readIn.read((char*)&addWorker, sizeof(Worker))) {
		size++;
	}
	readIn.close();

	Worker* temp = new Worker[size];
	int i = 0;
	fstream readList(nameFile, ios::binary | ios::in);
	while (readList.read((char*)&addWorker, sizeof(Worker))) {
		temp[i] = addWorker;
		i++;
	}
	readList.close();

	for (int i = 0; i < size; i++) {
		bool flag = true;
		for (int j = 0; j < size - (i + 1); j++) {
			if (temp[j].getLogin() > temp[j + 1].getLogin()) {
				flag = false;
				swap(temp[j], temp[j + 1]);
			}
		}
		if (flag) { break; }
	}
	for (int j = 0; j < size; j++) {
		cout << "\nID - " << temp[j].getID() << "\tИмя - " << temp[j].getLogin() << "\tДолжность - " << temp[j].getPosition() << "\tЗар.плата - " << temp[j].getSalary() << endl;
	}
	int request;
	cout << "Чтобы вернуться назад введите любую цифру...";
	cin >> request;
	ViewWorkerMenu();
}

void sortCarpet() {
	int size = 0;
	string nameFile = "CarpetList.bin";
	Carpet addCarpet{ (string)"", (string)"", 0.0, 0,0,false };
	fstream readIn(nameFile, ios::binary | ios::in);
	while (readIn.read((char*)&addCarpet, sizeof(Carpet))) {
		size++;
	}
	readIn.close();

	Carpet* temp = new Carpet[size];
	int i = 0;
	fstream readList(nameFile, ios::binary | ios::in);
	while (readList.read((char*)&addCarpet, sizeof(Carpet))) {
		temp[i] = addCarpet;
		i++;
	}
	readList.close();

	for (int i = 0; i < size; i++) {
		bool flag = true;
		for (int j = 0; j < size - (i + 1); j++) {
			if (temp[j].m_price > temp[j + 1].m_price) {
				flag = false;
				swap(temp[j], temp[j + 1]);
			}
		}
		if (flag) { break; }
	}
	for (int j = 0; j < size; j++) {
		cout << "\nID - " << temp[j].m_id << "\tНазвание - " << temp[j].m_name << "\tОписание - " << temp[j].m_describe << "\tЦена - " << temp[j].m_price << "\tКоличество - " << temp[j].m_number << endl;
	}
	int request;
	cout << "Чтобы вернуться назад введите любую цифру...";
	cin >> request;
	ViewCarpetMenu();
}

void editClient() {
	setlocale(LC_ALL, "");
	cout << "Введите id изменяемого клиента: ";
	int number;
	cin >> number;
	cout << "Введите имя клиента: ";
	string login;
	cin >> login;
	cout << "Введите пароль клиента: ";
	string password;
	cin >> password;
	Client dopClient{ (string)"", (string)"" };
	Client addClient{ login, password };
	string nameFile = "ClientList.bin";
	string nameAddFile = "ClientListAdd.bin";
	ofstream clientDopEdFile(nameAddFile, ios::binary | ios::out);
	fstream clientEdFile(nameFile, ios::binary | ios::in);
	while (clientEdFile.read((char*)&dopClient, sizeof(Client))) {
		if (dopClient.getID() == number) {
			clientDopEdFile.write((char*)&addClient, sizeof(Client));
		}
		else {
			clientDopEdFile.write((char*)&dopClient, sizeof(Client));
		}
	}
	clientEdFile.close();
	clientDopEdFile.close();

	ofstream inclientEdFile(nameFile, ios::binary | ios::out);
	fstream outclientDopEdFile(nameAddFile, ios::binary | ios::in);
	while (outclientDopEdFile.read((char*)&dopClient, sizeof(Client))) {
		inclientEdFile.write((char*)&dopClient, sizeof(Client));
	}
	inclientEdFile.close();
	outclientDopEdFile.close();
	ViewClientMenu();
}

void editWorker() {
	setlocale(LC_ALL, "");
	cout << "Введите id изменяемого сотрудника: ";
	int number;
	cin >> number;
	cout << "Введите имя сотрудника: ";
	string login;
	cin >> login;
	cout << "Введите пароль сотрудника: ";
	string password;
	cin >> password;
	cout << "Введите должность сотрудника: ";
	string position;
	cin >> position;
	cout << "Введите сумму зарплаты: ";
	double salary;
	cin >> salary;
	Worker dopWorker{ (string)"", (string)"", (string)"", 0.0 };
	Worker addWorker{ login, password, position, salary };
	string nameFile = "WorkerList.bin";
	string nameAddFile = "WorkerListAdd.bin";
	ofstream workerDopEdFile(nameAddFile, ios::binary | ios::out);
	fstream workerEdFile(nameFile, ios::binary | ios::in);
	while (workerEdFile.read((char*)&dopWorker, sizeof(Worker))) {
		if (dopWorker.getID() == number) {
			workerDopEdFile.write((char*)&addWorker, sizeof(Worker));
		}
		else {
			workerDopEdFile.write((char*)&dopWorker, sizeof(Worker));
		}
	}
	workerEdFile.close();
	workerDopEdFile.close();

	ofstream inworkerEdFile(nameFile, ios::binary | ios::out);
	fstream outworkerDopEdFile(nameAddFile, ios::binary | ios::in);
	while (outworkerDopEdFile.read((char*)&dopWorker, sizeof(Worker))) {
		inworkerEdFile.write((char*)&dopWorker, sizeof(Worker));
	}
	inworkerEdFile.close();
	outworkerDopEdFile.close();
	ViewWorkerMenu();
}

void editCarpet() {
	setlocale(LC_ALL, "");
	cout << "Введите название товара: ";
	string name;
	cin >> name;
	cout << "Введите описание товара: ";
	string describe;
	cin >> describe;
	cout << "Введите цену товара: ";
	double price;
	cin >> price;
	cout << "Введите количество товара: ";
	int number;
	cin >> number;
	bool isExist = false;
	if (number > 0) { isExist = true; }
	Carpet dopCarpet{ (string)"", (string)"", 0.0,0,0,false };
	Carpet addCarpet{ name, describe, price, number, isExist };
	string nameFile = "CarpetList.bin";
	string nameAddFile = "CarpetListAdd.bin";
	ofstream carpetDopEdFile(nameAddFile, ios::binary | ios::out);
	fstream carpetEdFile(nameFile, ios::binary | ios::in);
	while (carpetEdFile.read((char*)&dopCarpet, sizeof(Carpet))) {
		if (dopCarpet.m_id == number) {
			carpetDopEdFile.write((char*)&addCarpet, sizeof(Carpet));
		}
		else {
			carpetDopEdFile.write((char*)&dopCarpet, sizeof(Carpet));
		}
	}
	carpetEdFile.close();
	carpetDopEdFile.close();

	ofstream incarpetEdFile(nameFile, ios::binary | ios::out);
	fstream outcarpetDopEdFile(nameAddFile, ios::binary | ios::in);
	while (outcarpetDopEdFile.read((char*)&dopCarpet, sizeof(Carpet))) {
		incarpetEdFile.write((char*)&dopCarpet, sizeof(Carpet));
	}
	incarpetEdFile.close();
	outcarpetDopEdFile.close();
	ViewCarpetMenu();
}

void editOrder() {
	setlocale(LC_ALL, "");
	cout << "Введите id заказа: ";
	int idOrder;
	cin >> idOrder;
	cout << "Введите id клиента: ";
	int idClient;
	cin >> idClient;
	cout << "Введите id товара: ";
	int idCarpet;
	cin >> idCarpet;
	cout << "Введите количество товара: ";
	int number;
	cin >> number;
	Order dopOrder{ 0,0,0,time(0),0 };
	Order addOrder{ idOrder, idClient, idCarpet, time(0), number };
	string nameFile = "OrderList.bin";
	string nameAddFile = "OrderListAdd.bin";
	ofstream orderDopEdFile(nameAddFile, ios::binary | ios::out);
	fstream orderEdFile(nameFile, ios::binary | ios::in);
	while (orderEdFile.read((char*)&dopOrder, sizeof(Order))) {
		if (dopOrder.m_id == number) {
			orderDopEdFile.write((char*)&addOrder, sizeof(Order));
		}
		else {
			orderDopEdFile.write((char*)&dopOrder, sizeof(Order));
		}
	}
	orderEdFile.close();
	orderDopEdFile.close();

	ofstream inorderEdFile(nameFile, ios::binary | ios::out);
	fstream outorderDopEdFile(nameAddFile, ios::binary | ios::in);
	while (outorderDopEdFile.read((char*)&dopOrder, sizeof(Order))) {
		inorderEdFile.write((char*)&dopOrder, sizeof(Order));
	}
	inorderEdFile.close();
	outorderDopEdFile.close();
	ViewClientMenu();
}