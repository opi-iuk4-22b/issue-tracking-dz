#include "Worker.h"

Worker::Worker() {}

Worker::Worker(string login, string password, string position, double salary) {
	m_login = login;
	m_password = password;
	m_position = position;
	m_salary = salary;
}


void Worker::setPosition(string position) {
	m_position = position;
}

string Worker::getPosition() {
	return m_position;
}

void Worker::setSalary(double salary) {
	m_salary = salary;
}

double Worker::getSalary() {
	return m_salary;
}
