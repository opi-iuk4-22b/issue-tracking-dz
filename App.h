#ifndef APP_H
#define APP_H
#include "Carpet.h"
#include "Order.h"
#include "Client.h"
#include "Worker.h"

void addClient();
void addWorker();
void addCarpet();
void addOrder();

void removeClient();
void removeWorker();
void removeCarpet();
void removeOrder();

void sortClient();
void sortWorker();
void sortCarpet();
void sortOrder();

void editClient();
void editWorker();
void editCarpet();
void editOrder();

void filterClient();
void filterWorker();
void filterCarpet();
void filterOrder();

#endif