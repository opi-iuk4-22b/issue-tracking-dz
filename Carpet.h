#include <iostream>
#ifndef CARPET_MENU_H
#define CARPET_MENU_H
using namespace std;

struct Carpet {
	string m_name{ (string)"None" };
	string m_describe{ (string)"None" };
	double m_price{ 0.0 };
	int m_id{ rand() % 9000 };
	int m_number{ 0 };
	bool m_isExist{ false };
};
#endif