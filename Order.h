#include <iostream>
#ifndef ORDER_MENU_H
#define ORDER_MENU_H
using namespace std;

struct Order {
	int m_id{ rand() % 9000 };
	int m_idUser{ 0 };
	int m_idCarpet{ 0 };
	time_t m_date{ };
	int m_kol{ 0 };
};
#endif