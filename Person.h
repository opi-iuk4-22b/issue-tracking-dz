#include <iostream>
#include  <string.h>
#ifndef PERSON_MENU_H
#define PERSON_MENU_H
using namespace std;

class Person {
protected:
	string m_login{ (string)"user" };
	string m_password{ (string)"user" };
	int m_id{ rand() % 9000 };
	bool m_isAdmin{ false };
public:
	Person();
	Person(string, string);
	string getLogin();
	string getPassword();
	int getID();
	void setPassword(string);
	void setLogin(string);
};
#endif