#include "Person.h"
#ifndef WORKER_MENU_H
#define WORKER_MENU_H
using namespace std;

class Worker : public Person {
	string m_position{};
	double m_salary{};
public:
	Worker();
	Worker(string, string, string, double);
	void setSalary(double);
	double getSalary();
	void setPosition(string);
	string getPosition();
};
#endif