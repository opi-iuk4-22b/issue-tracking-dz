#include "Person.h"

Person::Person() {}

Person::Person(string login, string password) : m_login(login), m_password(password) {}

string Person::getLogin() {
	return m_login;
}

string Person::getPassword() {
	return m_password;
}

int Person::getID() {
	return m_id;
}
void Person::setPassword(string password) {
	m_password = password;
}
void Person::setLogin(string login) {
	m_login = login;
}